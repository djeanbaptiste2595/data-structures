"""Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
You may assume that each input would have exactly one solution, and you may not use the same element twice.
You can return the answer in any order.



Example 1:

Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
Example 2:

Input: nums = [3,2,4], target = 6
Output: [1,2]
Example 3:

Input: nums = [3,3], target = 6
Output: [0,1]


Constraints:

2 <= nums.length <= 104
-109 <= nums[i] <= 109
-109 <= target <= 109
Only one valid answer exists.


Follow-up: Can you come up with an algorithm that is less than O(n2) time complexity?

class Solution:
    def twoSum(self, num: List[int], target: int) -> List[int]:
        prevMap = {} #"""


class Solution:
    def twoSum(self, nums, target):
        for i in range(len(nums)):
            for j in range(i+1, len(nums)):
                sum = nums[i] + nums[j]
                if sum == target:
                    return [i, j]
nums = [2,7,11,15]
target = 9
result = Solution().twoSum(nums, target)
print(result)  # Output: [0, 1]

# Example 2
nums2 = [3, 5, 7, 9]
target2 = 12
result2 = Solution().twoSum(nums2, target2)
print(result2)  # Output: [0, 2]

#example 3
nums3 = [1, 2, 3, 4, 5]
target3 = 9
result3 = Solution().twoSum(nums3, target3)
print(result3)  # Output: [3, 4]

# Example 4
nums4 = [0, 1, 2, 3, 4]
target4 = 6
result4 = Solution().twoSum(nums4, target4)
print(result4)  # Output: [2, 4]
